package main

// CREATE DATABASE `options_history` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */
/*
CREATE TABLE `expirations` (
  `symbol` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` date NOT NULL,
  `class` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`symbol`,`expiration`,`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
*/
import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gocarina/gocsv"
)

const (
	tableDefinition = `CREATE TABLE IF NOT EXISTS %table_name% (
		symbol varchar(20),
		exchange varchar(20),
		company_name varchar(20),
		trade_date date,
		stock_price_close float,
		option_symbol varchar(30),
		option_expiration date,
		strike float,
		call_put varchar(5),
		style varchar(10),
		ask float,
		bid float,
		mean_price float,
		settlement float,
		iv float,
		volume int,
		open_interest int,
		stock_price_for_iv float,
		forward_price float,
		delta float,
		vega float,
		gamma float,
		theta float,
		rho float,
		primary key(trade_date,strike,call_put)
	)`
)

type optionEntry struct {
	Symbol           string  `csv:"symbol"`
	Exchange         string  `csv:"exchange"`
	CompanyName      string  `csv:"company_name"`
	Date             string  `csv:"date"`
	StockPriceClose  float32 `csv:"stock_price_close"`
	OptionSymbol     string  `csv:"option_symbol"`
	OptionExpiration string  `csv:"option_expiration"`
	Strike           float32 `csv:"strike"`
	CallPut          string  `csv:"call_put"`
	Style            string  `csv:"style"`
	Ask              float32 `csv:"ask"`
	Bid              float32 `csv:"bid"`
	MeanPrice        float32 `csv:"mean_price"`
	Settlement       float32 `csv:"settlement"`
	IV               float32 `csv:"iv"`
	Volume           int     `csv:"volume"`
	OpenInterest     int     `csv:"open_interest"`
	StockPriceForIV  float32 `csv:"stock_price_for_iv"`
	ForwardPrice     float32 `csv:"forward_price"`
	Isinterpolated   string  `csv:"isinterpolated"`
	Delta            float32 `csv:"delta"`
	Vega             float32 `csv:"vega"`
	Gamma            float32 `csv:"gamma"`
	Theta            float32 `csv:"theta"`
	Rho              float32 `csv:"rho"`
}

var tables = make(map[string]bool)
var mutex = &sync.Mutex{}
var db *sql.DB

func importData(data *[]*optionEntry) {

	lastTable := ""
	var buffer bytes.Buffer
	del := ""
	counter := 0

	for _, entry := range *data {
		d, err := time.Parse("01/02/2006", entry.OptionExpiration)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		td, err := time.Parse("01/02/2006", entry.Date)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		tableName := strings.ToLower(entry.Symbol + "_" + d.Format("20060102"))
		class := strings.Split(entry.OptionSymbol, " ")[0]

		mutex.Lock()
		if _, ok := tables[tableName]; !ok {
			fmt.Println(tableName)
			db.Exec(fmt.Sprintf("INSERT IGNORE INTO expirations (symbol,expiration,class) VALUES('%s','%s','%s')", entry.Symbol, d.Format("20060102"), class))
			db.Exec(strings.Replace(tableDefinition, "%table_name%", tableName, -1))

			tables[tableName] = true
		}
		mutex.Unlock()

		if lastTable != "" && counter > 0 && tableName != lastTable {
			r, err := db.Exec(buffer.String())
			if err != nil {
				fmt.Println(err.Error())
			}

			wr, _ := r.RowsAffected()
			fmt.Println("new table, wrote ", wr)

			buffer.Reset()
			counter = 0
		}

		if counter == 0 {
			del = ""
			buffer.WriteString(fmt.Sprintf("INSERT IGNORE INTO %s (symbol, exchange, company_name, trade_date, stock_price_close, option_symbol, option_expiration, strike, call_put, style, ask, bid, mean_price, settlement, iV, volume, open_interest, stock_price_for_iv, forward_price, delta, vega, gamma, theta, rho) VALUES ", tableName))
		} else {
			del = ","
		}

		buffer.WriteString(fmt.Sprintf("%s('%s','%s','%s','%s',%f,'%s','%s','%f','%s','%s','%f','%f','%f','%f','%f','%d','%d','%f','%f','%f','%f','%f','%f','%f')", del, entry.Symbol, entry.Exchange, entry.CompanyName, td.Format("20060102"), entry.StockPriceClose, entry.OptionSymbol, d.Format("20060102"), entry.Strike, entry.CallPut, entry.Style, entry.Ask, entry.Bid, entry.MeanPrice, entry.Settlement, entry.IV, entry.Volume, entry.OpenInterest, entry.StockPriceForIV, entry.ForwardPrice, entry.Delta, entry.Vega, entry.Gamma, entry.Theta, entry.Rho))

		counter++
		lastTable = tableName

	}

	if counter > 0 {
		r, err := db.Exec(buffer.String())
		if err != nil {
			fmt.Println(err.Error())
		}
		wr, _ := r.RowsAffected()
		fmt.Println("end, wrote ", wr)
	}

}

func processFile(file string) {

	dataFile, err := os.OpenFile(file, os.O_RDONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer dataFile.Close()

	reader := bufio.NewReader(dataFile)

	firstLine, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for {

		var buffer bytes.Buffer
		buffer.WriteString(firstLine)

		eof := false
		for i := 0; i < 10000; i++ {

			line, err := reader.ReadString('\n')
			if err != nil {
				if err == io.EOF {
					eof = true
					break
				}
				panic(err)
			}

			buffer.WriteString(line)
		}

		oe := []*optionEntry{}

		if err := gocsv.UnmarshalBytes(buffer.Bytes(), &oe); err != nil {
			panic(err)
		}

		importData(&oe)

		if eof {
			break
		}
	}
}

func main() {

	dataPath := flag.String("input", "d:/tmp_data/options/", "input folder")
	mysqlStr := flag.String("mysql", "root:password@tcp(127.0.0.1:3306)/options_history", "mysql dns")

	flag.Parse()

	var err error
	db, err = sql.Open("mysql", *mysqlStr)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Connection Established")
	}
	defer db.Close()

	files, err := ioutil.ReadDir(*dataPath)
	if err != nil {
		log.Fatal(err)
	}

	var sem = make(chan int, 8)
	defer close(sem)
	wg := sync.WaitGroup{}

	for _, f := range files {
		if !strings.Contains(f.Name(), "_1545") {
			continue
		}

		wg.Add(1)
		sem <- 1
		go func(name string) {
			processFile(*dataPath + name)
			<-sem
			wg.Done()
		}(f.Name())
	}

	wg.Wait()
}
