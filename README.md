# ivol-options-importer

Expaction is an existing mysql database with the schema below. The tool will create for each expiration a new table. It imports only the 15:45 snapshot data from ivolatility. So there is no time field in the database, this behavior could be changed. You can name the database as you want, the tool just has to be called with the right dns string.

### database
    CREATE DATABASE `options_history` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

### table expirations
	
    CREATE TABLE `expirations` (
	    `symbol` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
	    `expiration` date NOT NULL,
	    `class` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
	    PRIMARY KEY (`symbol`,`expiration`,`class`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

### run the tool
The csv files have to be unzipped into a folder, this folder has to be given as a parameter to the tool.

    ./ivol-options-importer -input="/tmp/unzipped/" -mysql="user:password@tcp(127.0.0.1:3306)/options_history"